# merchant-center

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## 表单验证
#### 1、规则

    参考网址：https://blog.csdn.net/qq_35909852/article/details/78725207

    在每个form-item里加上prop属性，并且与v-model的值相同
    <Form v-if="showDialog" ref="formItem" :model="formItem" :label-width="80" :rules="ruleValidate">
    
    data() {
        let validatestatus = function(rule, value, callback){
            if(value < 0) {
                return callback(new Error("请选择账号状态"));
            }else {
                callback();
            }
        };
        return {
            ruleValidate: {
                name: [
                    { required: true, message: 'The name cannot be empty', trigger: 'blur' }
                ],
                age: [
                    {required: true, type: 'number', message: '年龄需填写数字', trigger: 'blur',min: 1 }
                ],
                address: [
                    {required: true,type: 'string', min: 2, message: '至少2个字', trigger: 'blur' }
                ],
                status: [
                   { required: true, message: '请选择账号状态', trigger: 'blur'},
                   { validator: validatestatus, trigger: 'change'}
                ]
            }
        }
    }


    
    
    
#### 2、关于iview、element-ui重置表单并清除校验的方法
    
    参考网址：https://www.cnblogs.com/zhusf/p/10449127.html
    
    重置表单：this.$refs[formData].resetFields();   formData是form的ref值
    表单清除校验：只需要在From标签上加上v-if="showDialog"这句代码，当关闭弹框时showDialog=false，再次打开弹框是showDialog置为true，这样每次打开弹框它都会生成一个新的表单

## 表格增删改查
    弹框子组件调用列表父组件 this.$parent.方法名/data字段

## 国际化
    参考网址：https://blog.csdn.net/dx18520548758/article/details/79045626
            https://blog.csdn.net/Fabulous1111/article/details/79493825
            https://blog.csdn.net/qq_35858830/article/details/86485366

    引用：
    1、npm install vue-i18n --save
    2、main.js

        import VueI18n from 'vue-i18n'
        //配置国际化
        Vue.use(VueI18n)
        //注册i18n实例并引入语言文件，文件格式等下解析
        const i18n = new VueI18n({
        locale: 'zh',
        messages: {
            'zh': require('@/assets/languages/zh.json'),
            'en': require('@/assets/languages/en.json')
        }
        })
        //在实例中引入
        new Vue({
        router,
        i18n,
        render: h => h(App)
        }).$mount('#app')

    3、在assets文件中创建languages文件夹 包括语言的json文件
        json文件格式 {
                        "title": {
                            "title": "商户中心",
                            "name": "权限管理",
                            "edit": "编辑"
                        }
                    }

    4、获取数据的几种方式：
        标签中直接添加元素：{{$t('title.title')}}
        placehold 中因为切换： :placeholder="$t('title.title')"
        js中英文切换：alert( this.$t('title.title') )

    5、参考网址：https://github.com/stzhongjie/vue-login

    6、解决 “TypeError: Cannot read property ‘_t’ of undefined” 是vue跟i18n之间的兼容问题
      解决方法：将
                Vue.use(iView)
              替换成
                Vue.use(iView, {
                    i18n: (path, options) => {
                        let value = i18n.t(path, options)
                        if (value !== null && value !== undefined) {
                        return value
                        }
                        return ''
                    }
                })
    
## Vuex

### 1、state:
    vuex中的数据源，我们需要保存的数据就保存在这里，可以在页面通过 this.$store.state来获取我们定义的数据；

###  2、Getters
   Getter相当于vue中的computed计算属性，getter 的返回值会根据它的依赖被缓存起来，且只有当它的依赖值发生了改变才会被重新计算，这里我们可以通过定义vuex的Getter来获取，Getters 可以用于监听、state中的值的变化，返回计算后的结果.

### 3、Mutations
    数据我们在页面是获取到了，但是如果我们需要修改怎么办？修改store中的值的唯一的方法就是提交mutation来修改。
    
### 4、Actions
    先定义actions提交mutation的函数,在actions中提交mutation再去修改状态值

### mapState、mapGetters、mapActions

## 封装axios
### 参考网址
    https://juejin.im/post/5b55c118f265da0f6f1aa354