module.exports = {
    publicPath: '/',
    indexPath: 'index.html',
    devServer: {
        port: 8080,
        proxy: {
            '/api': {
                target: 'http://47.105.117.33:5000/', // target host
                ws: true, // proxy websockets 
                changeOrigin: true, // needed for virtual hosted sites
                pathRewrite: {
                    '^/api': '/api' 
                }
            },
        }
    }
};