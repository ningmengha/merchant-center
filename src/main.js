import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import api from './http/api' //导入api接口
import iView from 'iview'
//import 'iview/dist/styles/iview.css'
import md5 from 'js-md5'
import '../my-theme/dist/iview.css'
//引入i18n国际化插件
import VueI18n from 'vue-i18n'
import en from 'iview/dist/locale/en-US'
import zh from 'iview/dist/locale/zh-CN'

//时间格式转换
import moment from 'moment'
Vue.prototype.$moment = moment

//将api挂载到vue的原型上
Vue.prototype.$api = api;


Vue.config.productionTip = false

//配置md5加密
//使用this.$md5('holle')
Vue.prototype.$md5 = md5

//配置公共url
//Axios.defaults.headers.post['Content-Type'] = 'application/json';
//Axios.defaults.baseURL = "http://localhost:9603/"
//http://localhost:9603/merUser/getMerMenuTree
//配置axios
//Vue.prototype.$axios = Axios;

//配置国际化
Vue.use(VueI18n)
Vue.locale = () => { };
const messages = {
  en: Object.assign(require('@/assets/languages/en.json'), en),
  zh: Object.assign(require('@/assets/languages/zh.json'), zh),
}
//注册i18n实例并引入语言文件，文件格式等下解析
const i18n = new VueI18n({
  locale: 'zh',
  messages
})


//配置iview
Vue.use(iView, {
  i18n: (path, options) => { 
    let value = i18n.t(path, options)
    if (value !== null && value !== undefined) { 
      return value
    }
    return ''
  }
})
console.log("process.env.VUE_APP_URL");
console.log(process.env.VUE_APP_URL);
new Vue({
  store,
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')
