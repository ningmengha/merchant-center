import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

//初始化时用sessionStore.getItem('token'),这样子刷新页面就无需重新登录
const state = {
    token: window.sessionStorage.getItem('token'),
    email: window.sessionStorage.getItem('email')
};

const mutations = {
    LOGIN: (state, data) => {
        //更改token的值
        state.token = data;
        window.sessionStorage.setItem('token',data);
    },
    LOGOUT: (state) => {
        //退出的时候清除token
        state.token = null;
        state.email = null;
        window.sessionStorage.removeItem('token');
        window.sessionStorage.removeItem('email');
    },
    EMAIL: (state, data) => {
        //把用户邮箱存起来
        state.email = data;
        window.sessionStorage.setItem('email',data);
    }
}

const actions = {
    UserLogin({ commit }, data) {
        commit('LOGIN',data)
    },
    UserLogout({ commit }) {
        commit('LOGOUT')
    },
    UserEmail({ commit }, data) {
        commit('EMAIL',data)
    }
}

export default new Vuex.Store({
    state,
    mutations,
    actions
});