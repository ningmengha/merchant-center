import Vue from 'vue'
import Router from 'vue-router'
import store from './store'
import Layout from './components/Layout.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.VUE_APP_BASE_API,
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: () => import('@/views/Login.vue')
    },
    {
      path: '/register',
      name: 'Register',
      component: () => import('@/views/Register.vue')
    },
    {
      path: '/sysUser',
      name: '130001044',//系统管理
      component: Layout,
      children: [{
        path: '/menu/menuTreePage',
        name: '1',
        component: () => import('@/views/systemmanage/sysuser/Authority.vue')
      },
      {
        path: '/sysUser/userListPage',
        name: '2',
        component: () => import('@/views/systemmanage/sysuser/User.vue')
      },
      {
        path: '/userlist',
        name: '3',
        component: () => import('@/views/user/UserList')
      }
      ]
    }
  ]
})

//注册全局钩子用来拦截导航
router.beforeEach((to, from, next) => {
  //获取store里面的token
  let token = store.state.token;
    if(token) {
      next();
    } else {
      if (to.path === '/login' || to.path === '/register') {
        next();
      } else { 
        next({
          path: '/login'
        });
      }
    }
})

export default router;
