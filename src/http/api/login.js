/**
 * 登录注册接口
 */
import base from './base'
import axios from '../axios'
import qs from 'qs'  //引入qs模块，用来序列化post类型的数据。根据需求是否导入qs模块

const login = {
    //用户注册
    userRegister(data) {
        return axios.post(`${base.sq}/api/register`, data);
    },
    //用户登录
    userLogin(data) {
        return axios.post(`${base.sq}/api/login`, data);
    }
}

export default login;