//登录、注册
import login from './login'

//用户信息
import user from './user'


//导出接口
export default {
    login,
    user
}