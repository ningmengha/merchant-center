/**
 * 登录注册接口
 */
import base from './base'
import axios from '../axios'
import qs from 'qs' //引入qs模块，用来序列化post类型的数据。根据需求是否导入qs模块

const user = {
    //获取用户
    getUser(data) {
        return axios.post(`${base.sq}/api/user`, data);
    },
    //修改用户
    updateUser(data) {
        return axios.post(`${base.sq}/api/updateUser`, data);
    },
    //删除用户
    delUser(data) {
        return axios.post(`${base.sq}/api/delUser`, data);
    }
}

export default user;