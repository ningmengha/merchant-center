import axios from 'axios'
import store from '../store'
import router from '../router'

/** 
 * 跳转登录页
 * 携带当前页面路由，以期在登录页面完成登录后返回当前页面
 */
const toLogin = () => { 
    router.replace({
        path: '/login',
        query: {
            redirect: router.currentRoute.fullPath
        }
    })
}

/** 
 * 请求失败后的错误统一处理
 * @param {number} status 请求失败的状态码
 */
const errorHandle = (status, other) => { 
    //状态码判断
    switch (status) { 
        //401：未登录状态，跳转登录页
        case 401:
            toLogin();
            break;
        //403 token过期
        //清除token并跳转登录页
        case 403:
            this.$Message.info('登录过期，请重新登录');
            this.$store.dispatch('UserLogout');
            setTimeout(() => {
                toLogin();
            }, 1000);
            break;
        //404请求不存在
        case 404:
            this.$Message.info('请求资源不存在');
            break;
        default:
            console.log(other);
    }
}
//创建axios实例
const instance = axios.create({
    timeout: 5000, //请求超过5s即超时返回错误
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    } // application/x-www-form-urlencoded
});

//request拦截器
instance.interceptors.request.use(config => {
        //判断是否存在token,如果存在的话，则每个http header都加上token
        if (store.state.token) {
            config.headers.Authorization = `token ${store.state.token}`;
        }
        return config;
    },
    error => Promise.error(error)
)

//response拦截器
instance.interceptors.response.use(
    res => res.status === 200 ? Promise.resolve(res) : Promise.reject(res),
    error => { //默认除了2XX之外的都是错误的，就会走这里
        const { response } = error;
        if (response) {
            //请求已发出，但是不在2XX的范围
            errorHandle(response.status, response.data.message);
            return Promise.reject(response);
        } else { 
            //处理断网的情况
            //eg：请求超时或断网时，更新state的network状态
            //network状态在app.vue中控制着一个全局的断网提示组件的显示隐藏
            //关于断网组件中的刷新重新获取数据，会在断网组件中说明
            if (!window.navigator.onLine) {
                //调用store中network状态改变的方法
            } else { 
                return Promise.reject(error);
            }
        }
    }
)

export default instance;
